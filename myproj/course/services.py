from rest_framework.response import Response
import yaml
import requests

class fromRestServer:
    def read_file(self, filename):
        #######################################################################
        # Function: read_file                                                 #
        # Parameters: filename - filename is name of yml config file.         #
        #                        No validation done on filename               #
        #                                                                     #
        # Output: python object or error message                              #
        #                                                                     #
        # Description: The function open the .yml config file in read mode.   #
        #       The function yaml.load converts a YAML document to a Python   #
        #       object and return python object or error message as received  #
        #              from the server.                                       #
        #                                                                     #
        # Author:                                                             #
        # Release Date:                                                       #
        #                                                                     #
        # Revision History:                                                   #
        # --------------------------------------------------------------------#
        # Date       Author      Ref    Revision Description                  #
        # ----------------------------------------_---------------------------#
        #                                                                     #
        #                                                                     #
        #######################################################################
        with open(filename, 'r') as stream:
            try:
                return (yaml.load(stream))
            except yaml.YAMLError as exc:
                print(exc)

    def get_courses(self):
        #######################################################################
        # Function: get_courses                                               #
        #                                                                     #
        #  Output: Response list of dictionary which contain the Open edX     #
        #          Courses detail.                                            #
        #                                                                     #
        #  Description: The function read server detail, courses api link to  #
        #          form URL and fetch courses.                                #
        #                                                                     #
        # Author:                                                             #
        # Release Date:                                                       #
        #                                                                     #
        # Revision History:                                                   #
        # --------------------------------------------------------------------#
        # Date       Author      Ref    Revision Description                  #
        # --------------------------------------------------------------------#
        #                                                                     #
        #                                                                     #
        #######################################################################
        config_dict = self.read_file('config.yml')
        url = config_dict['iitbx_server_details'] + config_dict['get_courses_api']

        response_error_message = {
            200: 'Ok',
            400: 'An invalid parameter was sent.',
            403: 'You do not have permission to masquerade as another user specifies a username other than their own.',
            404: 'The specified user does not exist, or the requesting user does not have permission.'
            }
        params = {}

        serverresponse = requests.get(url, params=params)
        print serverresponse, " - ", serverresponse.status_code

        course_list = []
        if serverresponse.status_code == 200:
            courses = serverresponse.json()
            course_list = courses['results']
        else:
            if (response_error_message.has_key(serverresponse.status_code)):
                if response_error_message[serverresponse.status_code] == "Ok":
                    courses = serverresponse.json()
                    course_list = courses['results']
                else:
                    print "<", serverresponse.status_code, " > ", response_error_message[serverresponse.status_code]
            else:
                print "<", serverresponse.status_code, "> Undocumented Error."
        return course_list
