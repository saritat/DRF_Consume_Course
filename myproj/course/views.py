# -*- coding: utf-8 -*-
# from __future__ import unicode_literals
from django.shortcuts import render, redirect
from course.models import edxcourses
from course.serializers import CourseSerializer, TestSerializer
from rest_framework import generics
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
import services
#from course.services import fromRestServer
# from rest_framework import status
'''
from rest_framework.decorators import api_view
from rest_framework.response import Response
'''
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404
from rest_framework import status



class testCourseList(generics.ListCreateAPIView):
    """
    **Use Case:**

        Get the courses information available in system.

    **Example requests**:

        GET /api/course/listNew/

    **Response Values**

        * Org: The name of organization.

        * course_id: The course id, used for identify each course.

        * number: course number.

        * pacing: instructor/self.

        * name: The course Name.

        * start: The course starting Date.

        * end: The course end Date.

        * enrollment_start: The enrollment starting Date.

        * enrollment_end: The enrollment end Date.

        * short_description: The description about course.

        * created: Time of course creation.

    **Returns**

        * 200 on success.
        * 400 if an invalid parameter was sent or the username was not provided
          for an authenticated request.
        * 403 if a user who does not have permission to masquerade as
          another user specifies a username other than their own.
        * 404 if the specified user does not exist, or the requesting user does
          not have permission to view their courses.

    Example response:

        [
          {
            "org": "edX",
            "course_id": "Course-S1:IITBombayX+TC101x+test1",
            "number": "TC101x",
            "pacing": "instructor",
            "name": "Data Structure",
            "start": "2015-07-17T12:00:00Z",
            "end": "2015-09-19T18:00:00Z",
            "enrollment_start": "2018-02-01 06:30:00Z",
            "enrollment_end": "2018-03-29 00:00:00Z",
            "short_description": "1 post Data Structure update"
            "created": "2018-02-26T10:58:06.667195Z",
            "media": {
                "course_image": {
                    "uri": ""
                }
            }
          },
        ]

    """
    queryset = edxcourses.objects.all()
    serializer_class = TestSerializer


class testCourseDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    **Use Case:**

        Put the course information for updating.

    **Example requests**:

        PUT /api/course/detailUpdate/<id>

    **Response Values**

        * Org: The name of organization.

        * course_id: The course id, used for identify each course.

        * number: course number.

        * pacing: instructor/self.

        * name: The course Name.

        * start: The course starting Date.

        * end: The course end Date.

        * enrollment_start: The enrollment starting Date.

        * enrollment_end: The enrollment end Date.

        * short_description: The description about course.

        * created: Time of course creation.

        * image_path: The path of image.

    **Returns**

    * 200 on success, with a display course details for updating.
    * 400 if an invalid parameter was sent or the username was not provided
      for an authenticated request.
    * 403 if a user who does not have permission to masquerade as
      another user specifies a username other than their own.
    * 404 if the specified user does not exist, or the requesting user does
      not have permission to view their courses.

    Example response:

        [
          {
            "org": "edX",
            "course_id": "Course-S1:IITBombayX+TC101x+test1",
            "number": "TC101x",
            "pacing": "instructor",
            "name": "Data Structure",
            "start": "2015-07-17T12:00:00Z",
            "end": "2015-09-19T18:00:00Z",
            "enrollment_start": "2018-02-01 06:30:00Z",
            "enrollment_end": "2018-03-29 00:00:00Z",
            "short_description": "1 post Data Structure update"
            "created": "2018-02-26T10:58:06.667195Z",
            "image_path":  ""
            }
          },
        ]

    """
    queryset = edxcourses.objects.all()
    serializer_class = TestSerializer


class coursesList(APIView):
    queryset = edxcourses.objects.all()
    serializer_class = TestSerializer
    # This class is define using APIView which can create, update, delete
    # and list courses
    # Issue: Nothing
    # Status: Well
    model = edxcourses
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'courses_list.html'

    def get(self, request):
        # print "Request in get of class CoursesList: ", request
        queryset = edxcourses.objects.all()
        return Response({'courses': queryset})

    def post(self, request):
        # First update and then insert Pending
        # print "Request in post of class CoursesList: ", request
        queryset = edxcourses.objects.all()
        restServer = services.fromRestServer()
        data = restServer.get_courses()

        for course in data:
            try:
                courseobj = get_object_or_404(edxcourses, course_id = course["course_id"])
                serializer = TestSerializer(courseobj, data = course)
                if serializer.is_valid():
                    serializer.save()
                else:
                    return Response(serializer.errors,
                        status = status.HTTP_400_BAD_REQUEST)
            except Exception as e:
                try:
                    serializer = TestSerializer(data=course)
                    if not serializer.is_valid():
                        return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
                    else:
                        serializer.save()
                        print "DATA SAVE SUCESSFULLY.\n"
                except Exception as e:
                    print "Exception in Inserting: ", e.message

        redirect('course-list')
        return Response({'courses': queryset})
        # return Response(serializer.data, status=status.HTTP_201_CREATED)

    def put(self, request, pk, format=None):
        # course = self.get_object(pk)
        # print "PUT OF CoursesList:",request.data
        courseobj = edxcourses.objects.get(id = pk)

        serializer = TestSerializer(courseobj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            print "\n\n DATA UPDATE SUCESSFULLY."
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ExtractCoursePost(generics.RetrieveUpdateDestroyAPIView,
  generics.ListCreateAPIView):
    # ExtractCoursePost(generics.ListCreateAPIView, generics.UpdateAPIView):
    # This class is define using generics APIView which can create,
    # update, delete and list courses
    # Issue: Template HTML Renderer is not working.
    # Status: Working well in python shell only pending due to above issue.
    print "CourseDetail"
    model = edxcourses
    serializer_class = CourseSerializer
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'courses.html'  # ../template/courses.html
    # data = services.get_courses()

    def perform_create(self):
        # print "Data in perform_create: ",data
        queryset = edxcourses.objects.all()
        data = services.get_courses()
        args = []
        for course in data:
            # queryset = edxcourses.objects.filter(
            #     coursename = course["coursename"])
            try:
                courseobj = get_object_or_404(edxcourses, course_id = course["course_id"])
                # Update
                self.perform_update(course)
            except Exception as e:
                try:
                    serializer_class = CourseSerializer(data = course)
                    if serializer_class.is_valid():
                        serializer_class.save()
                        print status.HTTP_201_CREATED, " - DATA SAVE..", course
                    else:
                        return Response(serializer_class.errors,
                            status = status.HTTP_400_BAD_REQUEST)
                except Exception as e:
                    print "Exception in Inserting: ", e.message
        redirect('course-list')
        return Response({'courses': queryset})

    def perform_update(self, course):
        print "Data in perform_update: ", course
        queryset = edxcourses.objects.get(course_id = course["course_id"])
        print "queryset", queryset
        serializer_class = CourseSerializer(queryset, data=course)
        if serializer_class.is_valid():
            serializer_class.save()
            print status.HTTP_205_CREATED, " - DATA Update..", course
            # return Response(serializer_class.data,
            #     status=status.HTTP_205_CREATED)
        else:
            # Update
            print status.HTTP_400_BAD_REQUEST, "- DATA NOT Update...", course
            return Response(serializer_class.errors,
                status=status.HTTP_400_BAD_REQUEST)
