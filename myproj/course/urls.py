from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from course import views

# API endpoints
urlpatterns = [
    url(r'^$',
        views.testCourseList.as_view(),
        name='test-course-list'),

    url(r'^course/listNew/$',
        views.testCourseList.as_view(),
        name='test-course-list'),

    url(r'^course/detailUpdate/(?P<pk>[0-9]+)/$', 
        views.testCourseDetail.as_view(),
        name='test-course-detail'),

    url(r'^courses/fetch/$',
        views.coursesList.as_view(),
        name='course-list'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
