from rest_framework import serializers
from django.contrib.auth.models import User
from course.models import edxcourses
# from datetime import datetime
import datetime
import dateutil.parser
from collections import *
import collections
from collections import OrderedDict


class TestSerializer(serializers.ModelSerializer):
    model = edxcourses
    # Mapping of fields
    number = serializers.Field(source='course')
    pacing = serializers.Field(source='name')
    name = serializers.Field(source='coursename')
    start = serializers.Field(source='coursestart')
    end = serializers.Field(source='courseend')
    enrollment_start = serializers.Field(source='enrollstart')
    enrollment_end = serializers.Field(source='enrollend')
    image_path = serializers.Field(source='image')
    # course_attr['image'] = server_url[:-1]
    #     + course_info['media']['course_image']['uri']
    short_description = serializers.Field(source='coursesubtitle')

    class Meta:
        model = edxcourses
        # fields = '__all__'
        # fields = ('id', 'org', 'course_id','course', 'name', 'coursename',
        #     'coursestart', 'courseend', 'enrollstart', 'enrollend',
        #     'coursesubtitle' )
        fields = (
            'org', 'course_id', 'number', 'pacing', 'name', 'start', 'end',
            'enrollment_start', 'enrollment_end', 'short_description',
            'created', 'image_path',
            )

        read_only_fields = ('id', 'created')
        # write_only_fields = ('image_path')
        required_fields = (
            'course_id'
            )
        # 'read_only':True, 'write_only': True
        # extra_kwargs = {
        #     'course_id': {'required': True},
        #     'image_path': {'write_only': True},
        #     'password': {'write_only': True}
        # }
        # extra_kwargs = {
        #     'course_id': {'required': True},
        #     'image_path': {'write_only': True},
        # }
        extra_kwargs = {field: {'required': True} for field in required_fields}

    def validate(self, data):
        # print "Data  in validate function is:\n",data
        """
        Check course_id.
        """
        try:
            if data['course_id'] == "":
                raise serializers.ValidationError(
                    "Not allow course_id is Blank."
                    )
        except Exception as e:
            print "\nException in validate: ", e.message
            raise serializers.ValidationError("Not allow course_id is Blank.")

        return data

    def create(self, validated_data):
        try:
            if validated_data["course_id"] is not None:
                course_obj = edxcourses.objects.filter(
                    course_id=validated_data["course_id"]).first()
                if course_obj is not None:
                    return self.update(course_obj, validated_data)

            try:
                if validated_data["coursestart"]:
                    validated_data["coursestart"] = dateutil.parser.parse(
                        validated_data["coursestart"]).replace(tzinfo=None)
                else:
                    validated_data["coursestart"] = None
            except Exception as e:
                #print "\nException in coursestart: ", e.message
                message = "\nException in coursestart: " + e.message
                raise serializers.ValidationError(message)

            try:
                if validated_data["courseend"]:
                    validated_data["courseend"] = dateutil.parser.parse(
                        validated_data["courseend"]).replace(tzinfo=None)
                else:
                    validated_data["courseend"] = None
            except Exception as e:
                message = "\nException in courseend: " + e.message
                raise serializers.ValidationError(message)

            try:
                if validated_data["enrollstart"]:
                    validated_data["enrollstart"] = dateutil.parser.parse(
                        validated_data["enrollstart"]).replace(tzinfo=None)
                else:
                    validated_data["enrollstart"] = None
            except Exception as e:
                # print "\nException in enrollstart: ", e.message
                message = "\nException in enrollstart: " + e.message
                raise serializers.ValidationError(message)

            try:
                if validated_data["enrollend"]:
                    validated_data["enrollend"] = dateutil.parser.parse(
                        validated_data["enrollend"]).replace(tzinfo=None)
                else:
                    validated_data["enrollend"] = None
            except Exception as e:
                # print "\nException in enrollend: ", e.message
                message = "\nException in enrollend: " + e.message
                raise serializers.ValidationError(message)

            return edxcourses.objects.create(**validated_data)

        except Exception as e:
            print "\nException in Creating: ", e.message

    def strToDate(self, strDate, instanceDate, flag):
        """
        This function is used to convert string date strdate to model instance.
        """
        try:
            if strDate:
                # _T
                if instanceDate:
                    # TT
                    if instanceDate.replace(tzinfo=None) != dateutil.parser.parse(strDate).replace(tzinfo=None):
                        instanceDate = dateutil.parser.parse(strDate).replace(tzinfo=None)
                        flag = True
                else:
                    # FT
                    instanceDate = dateutil.parser.parse(strDate).replace(tzinfo=None)
                    flag = True
            else:
                # _F
                if instanceDate:
                    # TF
                    instanceDate = None
                    flag = True
            return {"setDate": instanceDate, "flag": flag}
        except Exception as e:
            message = "Exception in date_update: " + e.message
            message += " in Date " + strDate + " of Type:" + type(strDate)
            raise serializers.ValidationError(message)

    def update(self, instance, validated_data):
        """
        This function Update the data in database on put request.
        Before update it check the changes if any then update.
        """
        try:
            flag = False
            if instance.org != validated_data["org"]:
                instance.org = validated_data["org"]
                flag = True

            if instance.course_id != validated_data["course_id"]:
                instance.course_id = validated_data["course_id"]
                flag = True

            if instance.course != validated_data["course"]:
                instance.course = validated_data["course"]
                flag = True

            if instance.name != validated_data["name"]:
                instance.name = validated_data["name"]
                flag = True

            if instance.coursename != validated_data["coursename"]:
                instance.coursename = validated_data["coursename"]
                flag = True

            dateupdate = self.strToDate(
                validated_data["coursestart"],
                instance.coursestart,
                flag)
            instance.coursestart = dateupdate["setDate"]
            flag = dateupdate["flag"]

            dateupdate = self.strToDate(
                validated_data["courseend"],
                instance.courseend, flag)
            instance.courseend = dateupdate["setDate"]
            flag = dateupdate["flag"]

            dateupdate = self.strToDate(
                validated_data["enrollstart"],
                instance.enrollstart, flag)
            instance.enrollstart = dateupdate["setDate"]
            flag = dateupdate["flag"]

            dateupdate = self.strToDate(
                validated_data["enrollend"],
                instance.enrollend, flag)
            instance.enrollend = dateupdate["setDate"]
            flag = dateupdate["flag"]

            if instance.coursesubtitle != validated_data["coursesubtitle"]:
                instance.coursesubtitle = validated_data["coursesubtitle"]
                flag = True

            if instance.image != validated_data["image"]:
                instance.image = validated_data["image"]
                flag = True

            # instance.__dict__.update(**validated_data)

            if flag is True:
                instance.save()
                print "Data Update... ", instance.course_id, "\n"
            else:
                print "Data exists already...", instance.course_id, "\n"

            return instance

        except Exception as e:
            message = "\nException in Updating: " + e.message
            raise serializers.ValidationError(message)

    def to_representation(self, obj):
        # return model_to_dict(instance)
        try:
            if isinstance(obj, edxcourses):
                coursestartdt = None
                courseenddt = None
                enrollstartdt = None
                enrollenddt = None

                if obj.coursestart is not None:
                    if isinstance(obj.coursestart, datetime.date):
                        coursestartdt = obj.coursestart.strftime("%Y-%m-%d %H:%M:%SZ")

                if obj.courseend is not None:
                    if isinstance(obj.courseend, datetime.date):
                        courseenddt = obj.courseend.strftime("%Y-%m-%d %H:%M:%SZ")

                if obj.enrollstart is not None:
                    if isinstance(obj.enrollstart, datetime.date):
                        enrollstartdt = obj.enrollstart.strftime("%Y-%m-%d %H:%M:%SZ")

                if obj.enrollend is not None:
                    if isinstance(obj.enrollend, datetime.date):
                        enrollenddt = obj.enrollend.strftime("%Y-%m-%d %H:%M:%SZ")

                # To display in Order Add following two lines in header
                # from collections import *
                # import collections
                get_data = OrderedDict()
                k_fields = ['id', 'org', 'course_id', 'number', 'pacing', 'name', 'start', 'end']
                k_fields = k_fields + ['enrollment_start', 'enrollment_end', 'short_description']
                k_fields = k_fields + [ 'created', 'media', 'my_field', 'image_path'] #, 'image_path'

                values = [obj.id, obj.org, obj.course_id, obj.course, obj.name, obj.coursename]
                values = values + [coursestartdt, courseenddt, enrollstartdt, enrollenddt]
                values = values + [obj.coursesubtitle, obj.created, {}, 'my_field', obj.image] #obj.image

                get_data = OrderedDict((field_name, values[d_count]) for d_count, field_name in enumerate(k_fields))
                get_data['media'] = {'course_image':{'uri':obj.image}}

                return get_data

        except Exception as e:
            # print "\nException in to_representation: ", e.message
            message = "\nException in to_representation: "+ e.message
            raise serializers.ValidationError(message)


    def to_internal_value(self, data):
        # print "In to_internal_value FOR post:\n", data
        org = data.get('org')
        course_id = data.get('course_id')
        course = data.get('number')
        name = data.get('pacing')
        coursename = data.get('name')
        coursestart = data.get('start')
        courseend = data.get('end')
        enrollstart = data.get('enrollment_start')
        enrollend = data.get('enrollment_end')
        coursesubtitle = data.get('short_description')
        try:
            image = data["media"]["course_image"]["uri"]
        except Exception as e:
            image = data.get('image_path')
        
        if not course_id:
            raise serializers.ValidationError({
                'course_id': 'This field is required.'  # + serializers.errors
            })

        return {
                    "org": org,
                    "course_id": course_id,
                    "course": course,
                    "coursename": coursename,
                    "name": name,
                    "coursestart": coursestart,
                    "courseend": courseend,
                    "enrollstart": enrollstart,
                    "enrollend": enrollend,
                    "coursesubtitle": coursesubtitle,
                    "image": image
                }


class CourseSerializer(serializers.ModelSerializer):
    model = edxcourses
    # Mapping of fields
    number = serializers.Field(source='course')
    pacing = serializers.Field(source='name')
    name = serializers.Field(source='coursename')
    start = serializers.Field(source='coursestart')
    end = serializers.Field(source='courseend')
    enrollment_start = serializers.Field(source='enrollstart')
    enrollment_end = serializers.Field(source='enrollend')
    # uri = serializers.Field(source='image')
    # course_attr['image'] = server_url[:-1]+course_info['media']['course_image']['uri']
    short_description = serializers.Field(source='coursesubtitle')

    class Meta:
        model = edxcourses
        '''
        # Original Fields
        fields = ('org', 'course_id','course', 'name', 'coursename',
            'coursestart', 'courseend', 'enrollstart', 'enrollend',
            'coursesubtitle' )
        '''
        fields = (
            'org', 'course_id', 'number', 'pacing', 'name', 'start', 'end',
            'enrollment_start', 'enrollment_end', 'short_description', 'created'
            )

        read_only_fields = ('id', 'created')
        required_fields = (
            'course_id'
            )
        extra_kwargs = {field: {'required': True} for field in required_fields}

    def validate(self, data):
        # print "Data  in validate function is:\n",data
        return data

    def create(self, validated_data):
        # print "Validate data  in Create function is:\n",validated_data
        return edxcourses.objects.create(**validated_data)

    def update(self, instance, validated_data):
        # print "Validate data  in update function is:\n",validated_data
        # instance.__dict__.update(**validated_data)
        '''
        # print "in update", validated_data
        instance.org = validated_data.get('org', instance.org)
        instance.course_id = validated_data.get('course_id', instance.course_id)
        # print "After  course_id"
        instance.course = validated_data.get('number', instance.course)
        # print "After  course"
        instance.name = validated_data.get('pacing', instance.name)
        # print "After  name"
        instance.coursename = validated_data.get('name', instance.coursename)
        # print "After  coursename"
        instance.coursestart = validated_data.get('start', instance.coursestart)
        # print "After  coursestart"
        instance.courseend = validated_data.get('end', instance.courseend)

        instance.enrollstart = validated_data.get('enrollment_start', instance.enrollstart)
        instance.enrollend = validated_data.get('enrollment_end', instance.enrollend)
        instance.coursesubtitle = validated_data["coursesubtitle"]=validated_data.get('short_description', instance.coursesubtitle)
        # instance.pc_label = validated_data.get('label', instance.pc_label)
        # instance.tag = validated_data.get('mytag', instance.tag)
        # instance.courseend = validated_data.get('end', instance.courseend)
        # instance.coursestart = validated_data.get('start', instance.coursestart)
        '''
        instance.org = validated_data["org"]
        instance.course_id = validated_data["course_id"]
        instance.course = validated_data["course"]
        instance.name = validated_data["name"]
        instance.coursename = validated_data["coursename"]

        try:
            instance.coursestart = None
            if validated_data["coursestart"] is not None or trim(validated_data["coursestart"]) == "":

                if isinstance(validated_data["coursestart"], datetime.date):
                    instance.coursestart = datetime.datetime.strftime(datetime.datetime.strptime(validated_data["coursestart"], "%Y-%m-%d %H:%M:%SZ"), '%Y-%m-%d %H:%M:%SZ').strftime("%Y-%m-%d %H:%M:%SZ")
                else:
                    instance.coursestart = validated_data["coursestart"]

                # instance.coursestart = dateutil.parser.parse(validated_data["coursestart"]).replace(tzinfo = None)
                # course_attr['coursestart'] = dateutil.parser.parse(course_info['start']).replace(tzinfo = None)

        except Exception as e:
            instance.coursestart = None

        try:
            instance.courseend = None
            if validated_data["courseend"] is not None or trim(validated_data["courseend"]) == "":

                if isinstance(validated_data["courseend"], datetime.date):
                    instance.courseend = datetime.datetime.strftime(datetime.datetime.strptime(validated_data["courseend"], "%Y-%m-%d %H:%M:%SZ"), '%Y-%m-%d %H:%M:%SZ').strftime("%Y-%m-%d %H:%M:%SZ")
                else:
                    instance.courseend = validated_data["courseend"]

                # instance.courseend = dateutil.parser.parse(validated_data["courseend"]).replace(tzinfo = None)
                # course_attr['courseend'] = dateutil.parser.parse(course_info['start']).replace(tzinfo = None)

        except Exception as e:
            instance.courseend = None

        try:
            instance.enrollstart = None
            if validated_data["enrollstart"] is not None or trim(validated_data["enrollstart"]) == "":
                '''
                if isinstance(validated_data["enrollstart"],datetime.date):
                    instance.enrollstart = datetime.datetime.strftime(datetime.datetime.strptime( validated_data["enrollstart"],"%Y-%m-%d %H:%M:%SZ"), '%Y-%m-%d %H:%M:%SZ').strftime("%Y-%m-%d %H:%M:%SZ")
                else:
                    instance.enrollstart = validated_data["enrollstart"]
                '''
                instance.enrollstart = dateutil.parser.parse(validated_data["enrollstart"]).replace(tzinfo=None)
                # course_attr['enrollstart'] = dateutil.parser.parse(course_info['start']).replace(tzinfo = None)

        except Exception as e:
            instance.enrollstart = None

        try:
            instance.enrollend = None
            if validated_data["enrollend"] is not None or trim(validated_data["enrollend"]) == "":

                if isinstance(validated_data["enrollend"], datetime.date):
                    instance.enrollend = datetime.datetime.strftime(datetime.datetime.strptime(validated_data["enrollend"], "%Y-%m-%d %H:%M:%SZ"), '%Y-%m-%d %H:%M:%SZ').strftime("%Y-%m-%d %H:%M:%SZ")
                else:
                    instance.enrollend = validated_data["enrollend"]

                # instance.enrollend = dateutil.parser.parse(validated_data["enrollend"]).replace(tzinfo = None)

        except Exception as e:
            instance.enrollend = None

        '''
        if isinstance(validated_data["coursestart"],datetime.date):
            instance.coursestart = datetime.datetime.strftime(datetime.datetime.strptime( validated_data["coursestart"],"%Y-%m-%d %H:%M:%SZ"), '%Y-%m-%d %H:%M:%SZ').strftime("%Y-%m-%d %H:%M:%SZ")
        else:
            instance.coursestart = validated_data["coursestart"]

        #validated_data["coursestart"].strftime("%Y-%m-%d %H:%M:%SZ")

        if isinstance(validated_data["courseend"],datetime.date):
            instance.courseend = datetime.datetime.strftime(datetime.datetime.strptime( validated_data["courseend"],"%Y-%m-%d %H:%M:%SZ"), '%Y-%m-%d %H:%M:%SZ').strftime("%Y-%m-%d %H:%M:%SZ")
        else:
            instance.courseend = validated_data["courseend"]
        #validated_data["courseend"].strftime("%Y-%m-%d %H:%M:%SZ")

        if isinstance(validated_data["enrollstart"],datetime.date):
            instance.enrollstart = datetime.datetime.strftime(datetime.datetime.strptime( validated_data["enrollstart"],"%Y-%m-%d %H:%M:%SZ"), '%Y-%m-%d %H:%M:%SZ').strftime("%Y-%m-%d %H:%M:%SZ")
        else:
            instance.enrollstart = validated_data["enrollstart"]
        #validated_data["enrollstart"].strftime("%Y-%m-%d %H:%M:%SZ")

        if isinstance(validated_data["enrollend"],datetime.date):
            instance.enrollend = datetime.datetime.strftime(datetime.datetime.strptime( validated_data["enrollend"],"%Y-%m-%d %H:%M:%SZ"), '%Y-%m-%d %H:%M:%SZ').strftime("%Y-%m-%d %H:%M:%SZ")
        else:
            instance.enrollend = validated_data["enrollend"]

        #validated_data["enrollend"].strftime("%Y-%m-%d %H:%M:%SZ")
        '''
        instance.coursesubtitle = validated_data["coursesubtitle"]
        instance.image = validated_data["image"]
        instance.save()

        # print "instance.org: ", instance.org
        # print "instance.course_id: ", instance.course_id
        # print "instance.course: ", instance.course
        # print "instance.name: ", instance.name
        # print "instance.coursename: ", instance.coursename
        # print "instance.coursestart: ", instance.coursestart#.strftime("%Y-%m-%d %H:%M:%SZ")
        # print "instance.courseend: ",instance.courseend#.strftime("%Y-%m-%d %H:%M:%SZ")
        # print "instance.enrollstart: ", instance.enrollstart#.strftime("%Y-%m-%d %H:%M:%SZ")
        # print "instance.enrollend: ", instance.enrollend#.strftime("%Y-%m-%d %H:%M:%SZ")
        # print "instance.coursesubtitle: ", instance.coursesubtitle
        # print "out update instance:\n", instance
        return instance

    def to_representation(self, obj):

        ###############################################################################
        # Function: to_representation                                                         #
        # Parameters: obj - It is object of Model.                 #
        #                                                                             #
        # Output: It is Dictionary(python Data Structure)                                      #
        #                                                                             #
        # Description: This function is used to convert the initial data type into a  #
        #              primitive, serializable datatype.                              #
        #                                                                             #
        # Author:                                    #
        # Release Date:                                                    #
        #                                                                             #
        # Revision History:                                                           #
        # ----------------------------------------------------------------------------#
        # Date       Author      Ref    Revision Description                          #
        # ----------------------------------------------------------------------------#
        #                                                                             #
        #                                                                             #
        ###############################################################################

        # print "In to_representation for get:\n",obj
        if isinstance(obj, edxcourses):
            '''
            'start':(obj.coursestart).strftime("%Y-%m-%d %H:%M:%SZ"), #datetime.strptime(obj.coursestart,"YYYY-MM-DD HH:MM[TZ]").strftime("%Y-%m-%d %H:%M:%SZ"),#datetime.strftime(obj.coursestart,"YYYY-MM-DD HH:MM[TZ]"),
                'end': (obj.courseend).strftime("%Y-%m-%d %H:%M:%SZ"),
                'enrollment_start': (obj.enrollstart).strftime("%Y-%m-%d %H:%M:%SZ"),
                'enrollment_end': (obj.enrollend).strftime("%Y-%m-%d %H:%M:%SZ"),
            'start':obj.coursestart,
            'end': obj.courseend,
            'enrollment_start': obj.enrollstart,
            'enrollment_end': obj.enrollend,

            '''
            # print "666 ",obj.coursestart
            coursestartdt = ""
            courseenddt = ""
            enrollstartdt = ""
            enrollenddt = ""
            if obj.coursestart is not None:
                if isinstance(obj.coursestart, datetime.date):
                    coursestartdt = obj.coursestart.strftime("%Y-%m-%d %H:%M:%SZ")
                else:
                    coursestartdt = datetime.datetime.strftime(datetime.datetime.strptime(obj.coursestart, "%Y-%m-%d %H:%M:%SZ"), '%Y-%m-%d %H:%M:%SZ')
                    # .strftime("%Y-%m-%d %H:%M:%SZ")

            if obj.courseend is not None:
                if isinstance(obj.courseend, datetime.date):
                    courseenddt = obj.courseend.strftime("%Y-%m-%d %H:%M:%SZ")
                else:
                    courseenddt = datetime.datetime.strftime(datetime.datetime.strptime(obj.courseend, "%Y-%m-%dT0%H:%M:%SZ"), '%Y-%m-%d %H:%M:%SZ')
                    # .strftime("%Y-%m-%d %H:%M:%SZ")

            # obj.courseend.strftime("%Y-%m-%d %H:%M:%SZ")
            # datetime.datetime.strftime(obj.courseend, '%Y-%m-%d %H:%M:%SZ'), #obj.courseend.strftime("%Y-%m-%d %H:%M:%SZ"),  # obj.courseend,

            if obj.enrollstart is not None:
                if isinstance(obj.enrollstart, datetime.date):
                    enrollstartdt = obj.enrollstart.strftime("%Y-%m-%d %H:%M:%SZ")
                else:
                    enrollstartdt = datetime.datetime.strftime(datetime.datetime.strptime(obj.enrollstart, "%Y-%m-%dT0%H:%M:%SZ"), '%Y-%m-%d %H:%M:%SZ').strftime("%Y-%m-%d %H:%M:%SZ")

            if obj.enrollend is not None:
                if isinstance(obj.enrollend, datetime.date):
                    enrollenddt = obj.enrollend.strftime("%Y-%m-%d %H:%M:%SZ")
                else:
                    enrollenddt = datetime.datetime.strftime(datetime.datetime.strptime(obj.enrollend, "%Y-%m-%dT0%H:%M:%SZ"), '%Y-%m-%d %H:%M:%SZ').strftime("%Y-%m-%d %H:%M:%SZ")

            return {
                'org': obj.org,
                'course_id': obj.course_id,
                'number': obj.course,
                'pacing': obj.name,
                'name': obj.coursename,
                'start': coursestartdt,
                'end': courseenddt,
                'enrollment_start': enrollstartdt,
                'enrollment_end': enrollenddt,
                'short_description': obj.coursesubtitle,
                'created': obj.created
                }
            # serializers.org: org
        elif isinstance(obj, dict):
            # print "obj in dict: ", obj
            return{
                'org': obj["org"],
                'course_id': obj["course_id"],
                'number': obj["course"],
                'pacing': obj["name"],
                'name': obj["coursename"],

                'start': "",  # obj["coursestart"].strftime("%Y-%m-%d %H:%M:%SZ"),#"", # obj["coursestart"],
                'end': obj["courseend"].strftime("%Y-%m-%d %H:%M:%SZ"),  # obj["courseend"],
                'enrollment_start': obj["enrollstart"].strftime("%Y-%m-%d %H:%M:%SZ"),  # # obj["enrollstart"],
                'enrollment_end': obj["enrollend"].strftime("%Y-%m-%d %H:%M:%SZ"),  # obj["enrollend"],
                'short_description': obj["coursesubtitle"],
                # 'created': obj["created"]
                }
        else:
            raise Exception('Unexpected type of tagged object')
        '''
        if isinstance(obj, edxcourses):
            serializer = CourseSerializer(obj)
        else:
            raise Exception('Unexpected type of tagged object')
        return serializer.data
        '''

    def to_internal_value(self, data):
        # Description: This function is called to restore a primitive datatype into its internal python representation. This method should raise a serializers. ValidationError if the data is invalid

        # print "In to_internal_value FOR post:\n", data
        org = data.get('org')
        course_id = data.get('course_id')
        course = data.get('number')
        name = data.get('pacing')
        coursename = data.get('name')

        coursestart = data.get('start')
        courseend = data.get('end')
        enrollstart = data.get('enrollment_start')
        enrollend = data.get('enrollment_end')
        coursesubtitle = data.get('short_description')
        # print " POST VALUE: org - ", data.get('org')
        # print " POST VALUE: course_id - ", data.get('course_id')
        # print " POST VALUE: course - ", data.get('course')
        # print " POST VALUE: name - ", data.get('pacing')
        # print " POST VALUE: coursename - ", data.get('name')
        # print " POST VALUE: coursestart - ", data.get('start')
        # print " POST VALUE: courseend - ", data.get('end')
        # print " POST VALUE: enrollstart - ", data.get('enrollment_start')
        # print " POST VALUE: enrollend - ", data.get('enrollment_end')
        # print " POST VALUE: coursesubtitle - ", data.get('short_description')

        # Validate the The post value( Mandatory )

        # if not org:
        #     raise serializers.ValidationError({
        #         'org': 'This field is required.'
        #     })

        if not course_id:
            raise serializers.ValidationError({
                'course_id': 'This field is required.'  # + serializers.errors
            })

        # if not course:
        #     raise serializers.ValidationError({
        #         'number': 'This field is required.'#+ serializers.errors
        #     })

        # if not name:
        #     raise serializers.ValidationError({
        #         'name': 'This field is required.'#+ serializers.errors
        #     })

        # if not coursename:
        #     raise serializers.ValidationError({
        #         'coursename': 'This field is required.'
        #     })

        # if not coursestart:
        #     raise serializers.ValidationError({
        #         'coursestart': 'This field is required.'
        #     })

        # if not courseend:
        #     raise serializers.ValidationError({
        #         'courseend': 'This field is required.'
        #     })

        # if not enrollstart:
        #     raise serializers.ValidationError({
        #         'enrollstart': 'This field is required.'
        #     })

        # if not enrollend:
        #     raise serializers.ValidationError({
        #         'enrollend': 'This field is required.',
        #         'coursesubtitle': 'This field is required.'
        #     })
        # print "End of to_internal_value."
        return {
                    "org": org,
                    "course_id": course_id,
                    "course": course,
                    "coursename": coursename,
                    "name": name,
                    "coursestart": coursestart,
                    "courseend": courseend,
                    "enrollstart": enrollstart,
                    "enrollend": enrollend,
                    "coursesubtitle": coursesubtitle
                }
