# -*- coding: utf-8 -*-
# from __future__ import unicode_literals
from django.db import models


# Create your models here.
class edxcourses(models.Model):
    # "id" serial NOT NULL PRIMARY KEY,
    # id = models.AutoField(primary_key=True)
    # id = models.BigIntegerField(null=True)
    tag = models.CharField(max_length=100, null=True)
    org = models.CharField(max_length=100, null=True)
    course = models.CharField(max_length=100, null=True)
    name = models.CharField(max_length=100, null=True)
    course_id = models.CharField(max_length=100, unique=True)
    coursename = models.CharField(max_length=100, null=True)
    enrollstart = models.DateTimeField(null=True)
    enrollend = models.DateTimeField(null=True)
    coursestart = models.DateTimeField(null=True)
    courseend = models.DateTimeField(null=True)
    image = models.CharField(max_length=200, null=True)
    instructor = models.CharField(max_length=50, null=True)
    head_label = models.CharField(max_length=100, null=True)
    pc_label = models.CharField(max_length=100, null=True)
    teacher_label = models.CharField(max_length=100, null=True)
    coursesubtitle = models.TextField(null=True)
    blended_mode = models.IntegerField(null=True)
    is_deleted = models.NullBooleanField(null=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('created',)

# class sample(models.Model):
#     id = models.BigAutoField(null=False, primary_key=True)
#     tag = models.CharField(max_length = 100,null = True)
#     org = models.CharField(max_length = 100,null = True)

#     class BigAutoField(AutoField):
#         description = _("Big (8 byte) integer")

#         def get_internal_type(self):
#             return "BigAutoField"

#         def rel_db_type(self, connection):
#             return BigIntegerField().db_type(connection=connection)
